﻿using System;
using System.Collections.Generic;

namespace ConsoleApp4
{
    public enum CellState
    {
        Empty = 0,
        X = 1,
        O = 2,
    }

    class Cell
    {
        private int _row;
        private int _column;
        private CellState _state;

        public int Row
        {
            get { return _row; }
        }
        
        public int Column
        {
            get { return _column; }
        }

        public Cell(int row, int column)
        {
            _row = row;
            _column = column;
            _state = CellState.Empty;
        }

        public CellState GetState()
        {
            return _state;
        }

        public string GetStateString()
        {
            if (_state == CellState.Empty)
            {
                return " ";
            }

            if (_state == CellState.X)
            {
                return "X";
            }

            if (_state == CellState.O)
            {
                return "O";
            }

            return " ";
        }

        public void SetState(CellState state)
        {
            _state = state;
        }
    }

    class Player
    {
        private List<Cell> _calculatedHorizontalCells = new List<Cell>();
        private List<Cell> _calculatedVerticalCells = new List<Cell>();
        private List<Cell> _callculateDiagonalCells1 = new List<Cell>();
        private List<Cell> _callculateDiagonalCells2 = new List<Cell>();
        private List<Cell> _potencionalHorizontalCells = new List<Cell>();
        private List<Cell> _potencionalVerticalCells = new List<Cell>();
        private List<Cell> _potencionalDiagonalCells1 = new List<Cell>();
        private List<Cell> _potencionalDiagonalCells2 = new List<Cell>();
        private int _impotention;
        private int _score;

        public int Score
        {
            get { return _score; }
        }

        private bool _isBot;
        private int _nutNumber;
        private string _nickname;

        private CellState _mark;

        public string Nickname
        {
            get { return _nickname; }
        }

        public CellState Mark
        {
            get { return _mark; }
        }

        public Player(int nutNumber, int markNumber, bool bot)
        {
            _isBot = bot;
            if (nutNumber == 1)
            {
                _nutNumber = 1;

                if (bot == true)
                {
                    _nickname = "Bot1";
                }
                else
                {
                    _nickname = "Player1";
                }
            }
            else
            {
                _nutNumber = 2;

                if (bot == true)
                {
                    _nickname = "Bot2";
                }
                else
                {
                    _nickname = "Player2";
                }
            }

            if (markNumber == 1)
            {
                _mark = CellState.X;
            }
            else
            {
                _mark = CellState.O;
            }
        }

        public Player OponentPlayer(int gameMod)
        {
            int oponentNumber;
            bool bot;
            int oponentMark;


            if (gameMod == 1)
            {
                bot = false;
                if (_nutNumber == 1)
                {
                    oponentNumber = 2;
                }
                else
                {
                    oponentNumber = 1;
                }

                if (_mark == CellState.X)
                {
                    oponentMark = 2;
                }
                else
                {
                    oponentMark = 1;
                }
            }
            else
            {
                bot = true;
                if (_nutNumber == 1)
                {
                    oponentNumber = 2;
                }
                else
                {
                    oponentNumber = 1;
                }

                if (_mark == CellState.X)
                {
                    oponentMark = 2;
                }
                else
                {
                    oponentMark = 1;
                }
            }

            Player oponent = new Player(oponentNumber, oponentMark, bot);

            return oponent;
        }

        public int[] MakeMove(Pole pole)
        {
            int[] result = new int[2];

            if (_isBot == false)
            {
                Console.WriteLine("введите строку, а засем столбец");
                result[0] = int.Parse(Console.ReadLine());
                result[1] = int.Parse(Console.ReadLine());
                return result;
            }
            else
            {
                Random rnd = new Random();
                Cell[] emptyCells = pole.GetAllCellsWithMark(CellState.Empty);
                int randomIndex = rnd.Next(0, emptyCells.Length);
                Cell cell = emptyCells[randomIndex];
                result[0] = cell.Row;
                result[1] = cell.Column;
                return result;
            }
        }


        public int GetPotetntioScore()
        {
            int result = Convert.ToInt32(_impotention * 0.5);
            return result;
        }

        public string PrintScore()
        {
            return $"{Mark} --> {_score}";
        }

        public void CallculateAllPoints(Pole pole)
        {
            CallculateDiagonale1(pole);
            CallculateDiagonale2(pole);
            CallculateVerticalPoints(pole);
            CallculateHorizontalPoints(pole);
        }

        public bool IsMoveExist(Pole pole)
        {
            _impotention = 0;
            if (
                EculOrEmptyDiagonal2Points(pole) == false
                & EculOrEmptyDiagonal1Points(pole) == false
                & EculOrEmptyVerticalPoints(pole) == false
                & EculOrEmptyHorizontalPoints(pole) == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool IsTrinityEcul(Cell cell1, Cell cell2, Cell cell3, CellState state)
        {
            CellState cell1State = cell1.GetState();
            CellState cell2State = cell2.GetState();
            CellState cell3State = cell3.GetState();
            if (cell1State == state && cell3State == state && cell2State == state)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsTrinityEculOrEmpty(Cell cell1, Cell cell2, Cell cell3, CellState state)
        {
            CellState cell1State = cell1.GetState();
            CellState cell2State = cell2.GetState();
            CellState cell3State = cell3.GetState();
            if ((cell1State == state || cell1State == CellState.Empty)
                && (cell2State == state || cell2State == CellState.Empty)
                && (cell3State == state || cell3State == CellState.Empty))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool EculOrEmptyHorizontalPoints(Pole pole)
        {
            _potencionalHorizontalCells.Clear();
            bool isHodVozmogen = false;
            for (int i = 0; i < pole.Colums; i++)
            {
                for (int j = 0; j < pole.Rows - 2; j++)
                {
                    if (IsTrinityEculOrEmpty(pole.Cells[i, j], pole.Cells[i, j + 1], pole.Cells[i, j + 2], Mark)
                        && _calculatedHorizontalCells.Contains(pole.Cells[i, j]) == false
                        && _calculatedHorizontalCells.Contains(pole.Cells[i, j + 1]) == false
                        && _calculatedHorizontalCells.Contains(pole.Cells[i, j + 2]) == false
                        && _potencionalHorizontalCells.Contains(pole.Cells[i, j]) == false
                        && _potencionalHorizontalCells.Contains(pole.Cells[i, j + 1]) == false
                        && _potencionalHorizontalCells.Contains(pole.Cells[i, j + 2]) == false)
                    {
                        _potencionalHorizontalCells.Clear();
                        _potencionalHorizontalCells.Add(pole.Cells[i, j]);
                        _potencionalHorizontalCells.Add(pole.Cells[i, j + 1]);
                        _potencionalHorizontalCells.Add(pole.Cells[i, j + 2]);
                        isHodVozmogen = true;
                        _impotention++;
                        j += 2;
                    }
                }
            }

            return isHodVozmogen;
        }

        private bool EculOrEmptyVerticalPoints(Pole pole)
        {
            _potencionalVerticalCells.Clear();
            bool isHodVozmogen = false;
            for (int j = 0; j < pole.Colums; j++)
            {
                for (int i = 0; i < pole.Rows - 2; i++)
                {
                    if (IsTrinityEculOrEmpty(pole.Cells[i, j], pole.Cells[i + 1, j], pole.Cells[i + 2, j], Mark)
                        && _calculatedVerticalCells.Contains(pole.Cells[i, j]) == false
                        && _calculatedVerticalCells.Contains(pole.Cells[i + 1, j]) == false
                        && _calculatedVerticalCells.Contains(pole.Cells[i + 2, j]) == false
                        && _potencionalVerticalCells.Contains(pole.Cells[i, j]) == false
                        && _potencionalVerticalCells.Contains(pole.Cells[i + 1, j]) == false
                        && _potencionalVerticalCells.Contains(pole.Cells[i + 2, j]) == false)
                    {
                        _potencionalVerticalCells.Clear();
                        _impotention++;
                        _potencionalVerticalCells.Add(pole.Cells[i, j]);
                        _potencionalVerticalCells.Add(pole.Cells[i + 1, j]);
                        _potencionalVerticalCells.Add(pole.Cells[i + 2, j]);
                        isHodVozmogen = true;
                        i += 2;
                    }
                }
            }

            return isHodVozmogen;
        }

        private bool EculOrEmptyDiagonal1Points(Pole pole)
        {
            _potencionalDiagonalCells1.Clear();
            bool isHodVozmogen = false;
            for (int i = 0; i < pole.Colums - 2; i++)
            {
                for (int j = 0; j < pole.Rows - 2; j++)
                {
                    if (IsTrinityEculOrEmpty(pole.Cells[i, j], pole.Cells[i + 1, j + 1], pole.Cells[i + 2, j + 2], Mark)
                        && _callculateDiagonalCells1.Contains(pole.Cells[i, j]) == false
                        && _callculateDiagonalCells1.Contains(pole.Cells[i + 1, j + 1]) == false
                        && _callculateDiagonalCells1.Contains(pole.Cells[i + 2, j + 2]) == false
                        && _potencionalDiagonalCells1.Contains(pole.Cells[i, j]) == false
                        && _potencionalDiagonalCells1.Contains(pole.Cells[i + 1, j + 1]) == false
                        && _potencionalDiagonalCells1.Contains(pole.Cells[i + 2, j + 2]) == false)
                    {
                        _potencionalDiagonalCells1.Clear();
                        isHodVozmogen = true;
                        _impotention++;
                        _potencionalDiagonalCells1.Add(pole.Cells[i, j]);
                        _potencionalDiagonalCells1.Add(pole.Cells[i + 1, j + 1]);
                        _potencionalDiagonalCells1.Add(pole.Cells[i + 2, j + 2]);
                    }
                }
            }

            return isHodVozmogen;
        }

        private bool EculOrEmptyDiagonal2Points(Pole pole)
        {
            _potencionalDiagonalCells2.Clear();
            bool isHodVozmogen = false;
            for (int i = 2; i < pole.Colums; i++)
            {
                for (int j = 0; j < pole.Rows - 2; j++)
                {
                    if (IsTrinityEculOrEmpty(pole.Cells[i, j], pole.Cells[i - 1, j + 1], pole.Cells[i - 2, j + 2], Mark)
                        && _callculateDiagonalCells2.Contains(pole.Cells[i, j]) == false
                        && _callculateDiagonalCells2.Contains(pole.Cells[i - 1, j + 1]) == false
                        && _callculateDiagonalCells2.Contains(pole.Cells[i - 2, j + 2]) == false
                        && _potencionalDiagonalCells2.Contains(pole.Cells[i, j]) == false
                        && _potencionalDiagonalCells2.Contains(pole.Cells[i - 1, j + 1]) == false
                        && _potencionalDiagonalCells2.Contains(pole.Cells[i - 2, j + 2]) == false)
                    {
                        _potencionalDiagonalCells2.Clear();
                        isHodVozmogen = true;
                        _impotention++;
                        _potencionalDiagonalCells2.Add(pole.Cells[i, j]);
                        _potencionalDiagonalCells2.Add(pole.Cells[i - 1, j + 1]);
                        _potencionalDiagonalCells2.Add(pole.Cells[i - 2, j + 2]);
                    }
                }
            }

            return isHodVozmogen;
        }

        private void CallculateHorizontalPoints(Pole pole)
        {
            for (int i = 0; i < pole.Colums; i++)
            {
                for (int j = 0; j < pole.Rows - 2; j++)
                {
                    if (IsTrinityEcul(pole.Cells[i, j], pole.Cells[i, j + 1], pole.Cells[i, j + 2], Mark)
                        && _calculatedHorizontalCells.Contains(pole.Cells[i, j]) == false
                        && _calculatedHorizontalCells.Contains(pole.Cells[i, j + 1]) == false
                        && _calculatedHorizontalCells.Contains(pole.Cells[i, j + 2]) == false)
                    {
                        _score++;
                        _calculatedHorizontalCells.Add(pole.Cells[i, j]);
                        _calculatedHorizontalCells.Add(pole.Cells[i, j + 1]);
                        _calculatedHorizontalCells.Add(pole.Cells[i, j + 2]);

                        j += 2;
                    }
                }
            }
        }

        private void CallculateVerticalPoints(Pole pole)
        {
            for (int j = 0; j < pole.Colums; j++)
            {
                for (int i = 0; i < pole.Rows - 2; i++)
                {
                    if (IsTrinityEcul(pole.Cells[i, j], pole.Cells[i + 1, j], pole.Cells[i + 2, j], Mark)
                        && _calculatedVerticalCells.Contains(pole.Cells[i, j]) == false
                        && _calculatedVerticalCells.Contains(pole.Cells[i + 1, j]) == false
                        && _calculatedVerticalCells.Contains(pole.Cells[i + 2, j]) == false)
                    {
                        _score++;
                        _calculatedVerticalCells.Add(pole.Cells[i, j]);
                        _calculatedVerticalCells.Add(pole.Cells[i + 1, j]);
                        _calculatedVerticalCells.Add(pole.Cells[i + 2, j]);

                        i += 2;
                    }
                }
            }
        }

        private void CallculateDiagonale1(Pole pole)
        {
            for (int i = 0; i < pole.Colums - 2; i++)
            {
                for (int j = 0; j < pole.Rows - 2; j++)
                {
                    if (IsTrinityEcul(pole.Cells[i, j], pole.Cells[i + 1, j + 1], pole.Cells[i + 2, j + 2], Mark)
                        && _callculateDiagonalCells1.Contains(pole.Cells[i, j]) == false
                        && _callculateDiagonalCells1.Contains(pole.Cells[i + 1, j + 1]) == false
                        && _callculateDiagonalCells1.Contains(pole.Cells[i + 2, j + 2]) == false)
                    {
                        _score++;
                        _callculateDiagonalCells1.Add(pole.Cells[i, j]);
                        _callculateDiagonalCells1.Add(pole.Cells[i + 1, j + 1]);
                        _callculateDiagonalCells1.Add(pole.Cells[i + 2, j + 2]);
                    }
                }
            }
        }

        private void CallculateDiagonale2(Pole pole)
        {
            for (int i = 2; i < pole.Colums; i++)
            {
                for (int j = 0; j < pole.Rows - 2; j++)
                {
                    if (IsTrinityEcul(pole.Cells[i, j], pole.Cells[i - 1, j + 1], pole.Cells[i - 2, j + 2], Mark)
                        && _callculateDiagonalCells2.Contains(pole.Cells[i, j]) == false
                        && _callculateDiagonalCells2.Contains(pole.Cells[i - 1, j + 1]) == false
                        && _callculateDiagonalCells2.Contains(pole.Cells[i - 2, j + 2]) == false)
                    {
                        _score++;
                        _callculateDiagonalCells2.Add(pole.Cells[i, j]);
                        _callculateDiagonalCells2.Add(pole.Cells[i - 1, j + 1]);
                        _callculateDiagonalCells2.Add(pole.Cells[i - 2, j + 2]);
                    }
                }
            }
        }
    }

    class Pole
    {
        private Cell[,] _cells;
        public int Rows;
        public int Colums;

        public Cell[,] Cells
        {
            get { return _cells; }
        }

        public Pole(int n)
        {
            Rows = n;
            Colums = n;
            _cells = GenerateCells(n, n);
        }

        public Cell[] GetAllCellsWithMark(CellState state)
        {
            List<Cell> result = new List<Cell>();
            foreach (Cell cell in Cells)
            {
                if (cell.GetState() == state)
                {
                    result.Add(cell);
                }
            }

            return result.ToArray();
        }

        public void PrintPoleWithCordinate()
        {
            for (int i = 0; i < Colums; i++)
            {
                for (int j = 0; j < Rows; j++)
                {
                    Console.Write($"({i}{j}) |{_cells[i, j].GetStateString()}|");
                }

                Console.WriteLine();
            }
        }

        public void PrintPolePustoe()
        {
            for (int i = 0; i < Colums; i++)
            {
                for (int j = 0; j < Rows; j++)
                {
                    Console.Write($"|{_cells[i, j].GetStateString()}|");
                }

                Console.WriteLine();
            }
        }

        private Cell[,] GenerateCells(int rows, int columns)
        {
            Cell[,] res = new Cell[rows, columns];
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    res[i, j] = new Cell(i, j);
                }
            }

            return res;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите размер поля n*n, где n челое, нечетное число от 3 до 100");
            int n = int.Parse(Console.ReadLine());

            Pole pole = new Pole(n);
            pole.PrintPoleWithCordinate();
            Player currentPlayer;

            Console.WriteLine("Желаете сыграть PvP(1) или PvE(2)?");
            int gameMode = int.Parse(Console.ReadLine());

            Console.WriteLine("Кто будет ходить первым?(1)(2) или Случайно(3)");
            int hod = int.Parse(Console.ReadLine());

            Console.WriteLine("Х или О (1)(2)");
            int mark = int.Parse(Console.ReadLine());

            Player playerOne = new Player(hod, mark, false);

            //Player playerOne = new Player(hod, mark, true);   Bot vs Bot

            Player playerTwo = playerOne.OponentPlayer(gameMode);

            if (hod == 1)
            {
                currentPlayer = playerOne;
            }
            else if (hod == 2)
            {
                currentPlayer = playerTwo;
            }
            else
            {
                Random rnd3 = new Random();
                int kor4 = rnd3.Next(1, 3);
                hod = kor4;
                if (hod == 2)
                {
                    currentPlayer = playerOne;
                }
                else
                {
                    currentPlayer = playerTwo;
                }
            }

            for (int f = 0; f < n * n; f++)
            {
                int kor1;
                int kor2;
                do
                {
                    Console.WriteLine($"{currentPlayer.Nickname} введите координаты ячейки");
                    int[] cellkor = currentPlayer.MakeMove(pole);
                    kor1 = cellkor[0];
                    kor2 = cellkor[1];
                } while (pole.Cells[kor1, kor2].GetState() != CellState.Empty);

                pole.Cells[kor1, kor2].SetState(currentPlayer.Mark);

                currentPlayer.CallculateAllPoints(pole);

                pole.PrintPolePustoe();

                Console.WriteLine(playerOne.PrintScore() + " : " + playerTwo.PrintScore());

                currentPlayer.PrintScore();


                int razniza = Math.Abs(playerOne.Score - playerTwo.Score);
                if (currentPlayer == playerOne)
                {
                    currentPlayer = playerTwo;
                }
                else
                {
                    currentPlayer = playerOne;
                }

                if (currentPlayer.IsMoveExist(pole) == false)
                {
                    Console.WriteLine($"{currentPlayer.Mark} - Нет ходов с комбинацией");
                    break;
                }


                if (razniza > currentPlayer.GetPotetntioScore())
                {
                    Console.WriteLine($"{currentPlayer.Mark} - Нельзя догнать по Очку");
                    break;
                }
            }

            Console.WriteLine("Игра заканчивается со счетом!");
            Console.WriteLine(playerOne.PrintScore() + " : " + playerTwo.PrintScore());
            Console.ReadKey();
        }
    }
}